package com.udemy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/concepts")
public class ConceptsController {

	private static final String CONCEPT_VIEW = "concept";

	//localhost:8080/concepts/request?nm=taby
	//localhost:8080/concepts/request?nm=seba
	@GetMapping("/request")
	public ModelAndView request(@RequestParam(name="nm", required=false, defaultValue="null") String name) {
		ModelAndView mov = new ModelAndView(CONCEPT_VIEW);
		mov.addObject("nm_in_model", name);
		return mov;
	}
	
	//localhost:8080/concepts/new_request/taby
	@GetMapping("/new_request/{user_name}")
	public ModelAndView newRequest(@PathVariable("user_name") String user) {
		ModelAndView mov = new ModelAndView(CONCEPT_VIEW);
		mov.addObject("nm_in_model", user);
		return mov;
	}

}
