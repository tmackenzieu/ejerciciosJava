package com.udemy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.model.User;

@Controller
@RequestMapping("/example")
public class ExampleController {

	public static final String EXAMPLE_VIEW = "example";

	/*
	 * Primera forma (re direcciones donde no hay que insertar datos o muy pocos)
	 */

	@GetMapping("/exampleString")
	public String exampleString(Model model) {
		model.addAttribute("listOfUsers", getUsers());
		return EXAMPLE_VIEW;
	}

	/*
	 * segunda forma ( Inserción de muchos datos)
	 */

	@GetMapping("/exampleMAV")
	public ModelAndView exampleMaV() {
		ModelAndView objMOV = new ModelAndView(EXAMPLE_VIEW);
		objMOV.addObject("listOfUsers", getUsers());
		return objMOV;
	}

	private List<User> getUsers() {
		List<User> listUsers = new ArrayList<>();
			listUsers.add(new User("taby", 26));
			listUsers.add(new User("sebastián", 30));
			listUsers.add(new User("geraldine", 24));
			listUsers.add(new User("francisca", 23));
		return listUsers;
	}
}
