package com.udemy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.model.User;

@Controller
@RequestMapping("/conceptsPost")
public class ConceptsPostController {
	
	private static final String FORM_VIEW = "form";
	private static final String RESULT_VIEW = "result";

	
	@GetMapping("/showForm")
	public String showForm(Model model) {
		model.addAttribute("userOfForm", new User());
		return FORM_VIEW;
	}
	
	@PostMapping("/adduser")
	public ModelAndView addUserToForm(@ModelAttribute("user") User user) {
		ModelAndView mav = new ModelAndView(RESULT_VIEW);
		mav.addObject("user", user);
		return mav;
		
	}

}
